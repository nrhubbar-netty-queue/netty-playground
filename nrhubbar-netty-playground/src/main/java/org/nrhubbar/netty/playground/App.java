package org.nrhubbar.netty.playground;

import lombok.extern.log4j.Log4j2;

/**
 * Hello world!
 */
@Log4j2
public final class App {
    /**
     * Runs the app.
     * @param args - command line args.
     */
    public static void main(final String[] args) {
        log.info("Hello World");
    }

    /**
     * Does nothing.
     */
    private App() {

    }
}
