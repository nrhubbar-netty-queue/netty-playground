/**
 * This package holds the main app.
 */
package org.nrhubbar.netty.playground;


import lombok.extern.log4j.Log4j2;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Unit test for simple App.
 */
@Log4j2
public class AppTest
{    /**
     * Rigourous Test :-)
     */
    @Test
    public void testApp()
    {
        log.info("Both file and stdout");
        log.trace("only file");
        Assert.assertTrue( true );
    }
}
